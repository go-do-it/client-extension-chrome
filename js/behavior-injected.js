// Bind toolbar buttons
if (document.querySelector(".gdi-js-product-play, .gdi-js-checkout-play, .gdi-js-etc-play")) {
    document.querySelector(".gdi-js-product-play, .gdi-js-checkout-play, .gdi-js-etc-play").onclick = function () {
        GDIonClickProductPlay();
    }
}

if (document.querySelector(".gdi-js-product-add")) {
    document.querySelector(".gdi-js-product-add").onclick = function () {
        GDIonClickProductAdd();
    }
}

if (document.querySelector(".gdi-js-product-info")) {
    document.querySelector(".gdi-js-product-info").onclick = function () {
        GDIonClickProductInfo();
    }
}

if (document.querySelector(".gdi-js-checkout-save")) {
    document.querySelector(".gdi-js-checkout-save").onclick = function () {
        GDIonClickCheckoutSave();
    }
}

// If the buying process started go do it until is finished
if (GDIgetPlay() == "1") {
    GDIonClickProductPlay();
}

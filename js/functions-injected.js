function GDIsendPost(route, params) {
    var http = new XMLHttpRequest();
    var paramStr = "";
    for (var i in params) {
        if (paramStr != "") {
            paramStr += "&";
        }
        paramStr += i + "=" + params[i];
    }

    http.open("POST", route, true);

    //Send the proper header information along with the request
    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    http.onreadystatechange = function () {//Call a function when the state changes.
        if (http.readyState == 4 && http.status == 200) {
            alert(http.responseText);
        }
    }
    http.send(paramStr);
}

function GDIgetStepNo() {
    if (typeof localStorage.GDIstep == 'undefined') {
        return {
            "index": 0,
            "state": "redirect",
        };
    }

    return JSON.parse(localStorage.getItem("GDIstep"));
}

function GDIsetStepNo(step) {
    localStorage.GDIstep = JSON.stringify(step);
}

function GDIgetPlay() {
    if (typeof localStorage.GDIplay == 'undefined') {
        return '0';
    }

    return localStorage.GDIplay;
}

function GDIsetPlay(state) {
    localStorage.GDIplay = state;
}

function GDIonClickProductPlay() {
    // Start playing
    GDIsetPlay("1");

    // Basic check that the site is up
    if (
        document.querySelector('.header-wrapper .header-logo')
        || document.querySelector('#masthead .navbar-brand img')
        || document.querySelector('.emg-fluid-logo img')
        || document.querySelector('#emg-body-overlay .emg-container img')
    ) {
        var stepIndex = GDIgetStepNo();
        var steps = [
            {
                "price": "39.99",
                "price_expected": "29.99",
                "url": "https://www.emag.ro/mouse-optic-spacer-800-dpi-usb-black-spmo-080/pd/D3F2YMBBM/"
            },
            {
                "price": "39.99",
                "price_expected": "29.99",
                "url": "https://www.emag.ro/mouse-microsoft-mobile-1850-wireless-negru-7mm-00002/pd/D6R3KYBBM/"
            }
        ];
        var stepsLength = steps.length;

        // Buy products
        if (stepIndex["index"] < stepsLength) {
            if (stepIndex["state"] == "redirect") {
                var stepCurrent = steps[stepIndex["index"]];
                stepIndex["state"] = "add";
                GDIsetStepNo(stepIndex);

                window.location = stepCurrent["url"];
            } else {
                setTimeout(function () {
                    stepIndex["index"] += 1;
                    stepIndex["state"] = "redirect";
                    GDIsetStepNo(stepIndex);

                    document.querySelector(".product-page-actions button[type=submit]").click();

                    GDIonClickProductPlay();
                }, GDIdelay);
            }

            return;
        }

        // Checkout
        if (stepIndex["index"] == stepsLength) {
            if (stepIndex["state"] == "redirect") {
                stepIndex["state"] = "add";
                GDIsetStepNo(stepIndex);

                window.location = "https://www.emag.ro/cart/checkout";
            } else {
                setTimeout(function () {
                    stepIndex["index"] += 1;
                    stepIndex["state"] = "redirect";
                    GDIsetStepNo(stepIndex);

                    document.querySelector("#mainShipping .shipping-methods-container #courierDeliveryRadio").click();
                    document.querySelector(".courier-delivery .address-list li.line-item input[value='6144859']").click();
                    document.querySelector(".payment-methods-list .line-item input[value='numerar']").click();
                    document.querySelector(".order-summary-full button[type='submit']").click();

                    GDIonClickProductPlay();
                }, GDIdelay);
            }

            return;
        }

        // Summary
        if (stepIndex["index"] == stepsLength + 1) {
            if (stepIndex["state"] == "redirect") {
                stepIndex["state"] = "add";
                GDIsetStepNo(stepIndex);

                window.location = "https://www.emag.ro/cart/summary";
            } else {
                setTimeout(function () {
                    GDIsetStepNo({
                        "index": 0,
                        "state": "redirect"
                    });
                    // Stop playing
                    GDIsetPlay("0");

                    document.querySelector(".summary-total-container button[type='submit']").click();
                    // alert("place order")
                }, GDIdelay);
            }
        }
    } else {
        // Retry
        setTimeout(function () {
            window.location = 'https://www.emag.ro?nocache=1&rand=' + parseInt(Math.random() * 1000);
        }, GDIdelay);
    }
}

function GDIonClickProductAdd() {
    GDIsendPost(GDIAjaxRoutes["product_add"], {
        "url": window.location.href,
        "price": EM.productDiscountedPrice,
        "price_expected": EM.productDiscountedPrice - 10
    });
}

function GDIonClickProductInfo() {
    alert("Add, save, play! Go do it!");
}

function GDIonClickCheckoutSave() {
    GDIsendPost(GDIAjaxRoutes["product_add"], {
        "shipping_method": document.querySelector('input[name="shipping[method]"]:checked').value,
        "shipping_courier_address_id": document.querySelector('input[name="shipping[courier][address_id]"]:checked').value,
        "payment_method": document.querySelector('input[name="payment[method]"]:checked').value,
    });
}

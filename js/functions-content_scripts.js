function GDIpathMatch(url, pattern) {
    var re = new RegExp(pattern);
    return re.test(url);
}

function GDIgetHtmlToolbarButtons(routeName, buttons) {
    var button;
    var buttonsHtml = "";

    for (var i in buttons) {
        button = buttons[i];
        buttonsHtml += '<img src="' + GDIButtons[button] + '" alt="" title="' + button + '" class="gdi-js-' + routeName + '-' + button + '" />';
    }

    return buttonsHtml;
}

function GDIgetHtmlToolbarContainer(html) {
    return '<div id="gdi-toolbar">\
                <div class="container">\
                    ' + html + '\
                </div>\
            </div>';
}

function GDIAppend(selector, html) {
    $(selector).append(html);
}

function GDIPrepend(selector, html) {
    $(selector).prepend(html);
}

function GDIInjectScript(source) {
    var html = '<script src="' + chrome.extension.getURL(source) + '"></script>';
    GDIPrepend("body", html);
}

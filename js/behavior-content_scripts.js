$(document).ready(function () {
    var url = window.location.href;
    var action;
    var html;

    // Create toolbar for specific routes
    for (var i in GDIActionsClient) {
        action = GDIActionsClient[i];
        if (GDIpathMatch(url, action["route_pattern"])) {
            html = GDIgetHtmlToolbarButtons(action["route_name"], action["buttons"]);
            html = GDIgetHtmlToolbarContainer(html);
            GDIAppend("body", html);
            break;
        }
    }

    // Inject JS script in order to access current page JS global vars
    GDIInjectScript("js/config-injected.js");
    GDIInjectScript("js/functions-injected.js");
    GDIInjectScript("js/behavior-injected.js");
});

var GDIActionsClient = [
    {
        "route_pattern": "/.+/pd/.+",
        "route_name": "product",
        "buttons": [
            "play",
            "add",
            "info"
        ]
    },
    {
        "route_pattern": "/cart/checkout",
        "route_name": "checkout",
        "buttons": [
            "play",
            "save",
            "info"
        ]
    },
    {
        "route_pattern": "/.*",
        "route_name": "etc",
        "buttons": [
            "play"
        ]
    }
];

var GDIButtons = {
    "add": chrome.extension.getURL("images/icons/add.png"),
    "delete": chrome.extension.getURL("images/icons/delete.png"),
    "info": chrome.extension.getURL("images/icons/info.png"),
    "pause": chrome.extension.getURL("images/icons/pause.png"),
    "play": chrome.extension.getURL("images/icons/play.png"),
    "save": chrome.extension.getURL("images/icons/save.png")
};

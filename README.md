# Go Do IT - Google Chrome Extension

Hello,

We are [AutoMate](https://hackathon.emag.ro/#project?id=19) and here is the source code for our [Hackaton](https://hackathon.emag.ro) Project, a
[Google Chrome](https://www.google.com/chrome/) [extension](https://chrome.google.com/webstore/category/extensions) named
[Go Do IT](https://gitlab.com/go-do-it/client-extension-chrome). On this repository we are experimenting with
the [Google Chrome Manifest File Format](https://developer.chrome.com/extensions/manifest),
[Google Chrome JavaScript APIs](https://developer.chrome.com/extensions/api_index) and the Google Chrome development
flow: from creation and installation to publishing the extension.

## Requirements

In order to run this project on your machine you have to install the following packages:

* [Git](https://git-scm.com/). Check out
[Git install on Ubuntu Server](http://www.liviubalan.com/git-install-on-ubuntu-server) for more details.
* [Google Chrome](https://www.google.com/chrome/). Check out
[Google Chrome on Wikipedia](https://en.wikipedia.org/wiki/Google_Chrome) for more details.

## Installation

After the Requirements section is checked, open a [console](https://en.wikipedia.org/wiki/Command-line_interface) and
follow this steps:

1. Clone the GitLab project repository:

 ```bash
 git clone git@gitlab.com:go-do-it/client-extension-chrome.git
 ```

2. Optionally, change the current working directory to `client-extension-chrome` in order to modify the source code
of the extension:

 ```bash
 cd client-extension-chrome/
 ```

3. Open [Google Chrome](https://www.google.com/chrome/) from your local operating system and click **Customize and
control Google Chrome** (top right button) / **More tools** / **Extensions** or simply open a new Google Chrome tab and
type:

 ```bash
 chrome://extensions/
 ```

4. Check **Developer mode** and click on **Load unpacked extension...** and choose to open the content of
**client-extension-chrome** from step 1 above.

5. Ensure that the extension is enabled by checking the **Enabled** option for **Go Do IT** extension.

## Configuration

Most the configuration settings that you want to modify are placed on
[manifest.json](https://gitlab.com/go-do-it/client-extension-chrome/blob/master/manifest.json) and
[_locales/en/messages.json](https://gitlab.com/go-do-it/client-extension-chrome/blob/master/_locales/en/messages.json)
files. Also some other resources such as [JavaScript](https://en.wikipedia.org/wiki/JavaScript) and
[CSS](https://en.wikipedia.org/wiki/Cascading_Style_Sheets) are used so check them out.

## Debugging

Based on the current step of the extension development you can use the following Google Chrome builtin tools:
* **Inspect**: **Right click** on the current section (toolbar icon, page, etc) and choose **Inspect**.
* **Extension options** from **chrome://extensions/** for the current extension: **Reload (Ctrl+R)**, **Details**,
**Inspect views**.
* If you're using [PhpStorm](https://www.jetbrains.com/phpstorm/) you can add the `chrome` external library in order to
increase your productivity. Follow this
[stackoverflow question](http://stackoverflow.com/questions/13997468/how-do-i-use-webstorm-for-chrome-extension-development)
for more info.

Thank you for cloning!  
Bye bye!